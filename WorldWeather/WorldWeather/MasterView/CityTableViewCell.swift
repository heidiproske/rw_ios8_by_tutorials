//
//  CityTableViewCell.swift
//  WorldWeather
//
//  Created by Heidi Proske on 2/24/15.
//  Copyright (c) 2015 RayWenderlich. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {

    @IBOutlet weak var cityImageView: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    
    var cityWeather: CityWeather? {
        didSet {
            configureCell()
        }
    }
    
    func configureCell() {
        cityImageView.image = cityWeather?.cityImage
        cityNameLabel.text = cityWeather?.name
    }

}
