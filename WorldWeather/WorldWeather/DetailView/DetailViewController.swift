/*
* Copyright (c) 2014 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import UIKit

class DetailViewController: UIViewController {
  
  // MARK: - IBOutlets
  @IBOutlet weak var weatherIconImageView: UIImageView!
  
  // MARK: - Properties
  var cityWeather: CityWeather? {
  didSet {
    // Update the view.
    if isViewLoaded() {
      configureView()
      provideDataToChildViewControllers()
    }
  }
  }
  
  // MARK: - Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    configureView()
    provideDataToChildViewControllers()
    // Prep the navigation item so the back button doesn't disappear
    navigationItem.leftItemsSupplementBackButton = true
    navigationItem.hidesBackButton = false
    
    configureTraitOverrideForSize(view.bounds.size)
  }
  
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        println("Rotated")
        // make sure our new method is called for rotations
        configureTraitOverrideForSize(size)
    }
    
  // MARK: - Utility methods
  private func configureView() {
    if let cityWeather = cityWeather {
      title = cityWeather.name
      weatherIconImageView.image = cityWeather.weather[0].status.weatherType.image
    }
  }
  
  private func provideDataToChildViewControllers() {
    for vc in childViewControllers {
      if let cityWeatherContainer = vc as? CityWeatherContainer {
        cityWeatherContainer.cityWeather = cityWeather
      }
    }
  }
    
    /*
    Utility method that takes a CGSize as an argument. If the height of this 
    size is less than 1000 then it creates an instance of UITraitCollection 
    with a Compact vertical size class
    */
    private func configureTraitOverrideForSize(size: CGSize) {
        var traitOverride: UITraitCollection?
        /*
        The value of 1000 was chosen empirically using the resizable simulator – altering 
        the height to discover the point at which the vertically regular layout no longer 
        works. This makes the device and view controller hierarchy agnostic.
        */
        if size.height < 1000 { // not an iPad
            println("configureTraitOverrideForSize() set COMPACT rcvd height \(size.height)")
            // we have our autolayout set to not install detail collection view for
            // vertical size class Compact, but an iPhone in portrait mode has vertical 
            // Regular which still doesn't provide enough space to display the extra 
            // collection view (containing full week of weather) so we want to override
            // the intrinsic trait collection to override the vertical class to Compact
            // to ensure the view is suppressed!
            println("Previous trait vert \(self.traitCollection.verticalSizeClass.rawValue)")
            traitOverride = UITraitCollection(verticalSizeClass: .Compact)
        }
        for vc in childViewControllers as [UIViewController] {
            /*
            Note we're providing a trait collection with only its vertical size-class set, 
            so this is the only trait that’ll be overridden. Providing nil resets any 
            previous overrides to return it to its default, i.e. deferring to the trait
            collection of the container’s parent.
            */
            println("Overriding the trait collection with \(traitOverride?.verticalSizeClass.rawValue)")
            setOverrideTraitCollection(traitOverride, forChildViewController: vc)
        }
    }
}

